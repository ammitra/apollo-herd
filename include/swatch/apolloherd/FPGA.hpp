#ifndef __FPGA_HPP__
#define __FPGA_HPP__

// determine which FPGA the ApolloCMFPGA class represents
enum class FPGA {
  KINTEX,
  VIRTEX
};

#endif  // __FPGA_HPP__